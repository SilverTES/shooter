﻿using Microsoft.Xna.Framework;
using Retro2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooter
{
    public class Shake
    {
        float _intensity;
        float _step;
        bool _isShake;

        public bool IsShake() { return _isShake; }

        public void SetIntensity(float intensity, float step = 2)
        {
            _intensity = intensity;
            _step = step;
            _isShake = true;
        }

        public Vector2 GetVector2()
        {
            Vector2 vec = new Vector2();

            if (_intensity > 0)
            {
                _isShake = true;

                vec.X = Misc.Rng.Next(-(int)_intensity, (int)_intensity);
                vec.Y = Misc.Rng.Next(-(int)_intensity, (int)_intensity);

                _intensity -= _step;

            }
            else
            {
                _isShake = false;
            }

            return vec;
        }
    }
}
