﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace Shooter
{
    public class Bullet : Node
    {
        #region

        public const int ZONE_BODY = 0; 

        float _vx, _vy;
        float _ax, _ay;

        float _velocity;
        float _acceleration;

        float _angle;
        float _speed;

        int _life;

        int _power;

        public Node _owner;
        public Color _color;

        #endregion

        public Bullet(Node owner, Color color, int life, int power,  float speed, float angle, float acceleration = 0)
        {

            _type = UID.Get<Bullet>();


            _owner = owner;
            _color = color;

            _life = life;
            _power = power;
            _angle = angle;

            if (acceleration == 0)
                _speed = _velocity = speed;
            else
            {
                _acceleration = acceleration;
                _velocity = 0;
                _speed = speed;
            }

            SetSize(_power, _power);

            SetCollideZone(ZONE_BODY, RectangleF.Empty);
        }

        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;
            UpdateRect();

            UpdateCollideZone(ZONE_BODY, _rect, true);

            _velocity += _velocity < _speed ? _acceleration : 0;

            //Collide.Zone zone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(ZONE_BODY), UID.Get<Bullet>(), Bullet.ZONE_BODY);

            //if (null != zone)
            //{
            //    if (zone._node.This<Bullet>()._owner != _owner)
            //    {
            //        new Explosion(Color.White, (float)1 / 2f, true)
            //            .SetPosition(_x, _y)
            //            .AppendTo(_parent);

            //        zone._node.KillMe();
            //        KillMe();
            //    }
            //}


            _vx = Geo.GetVector(_angle).X * _velocity;
            _vy = Geo.GetVector(_angle).Y * _velocity;

            _x += _vx;
            _y += _vy;

            --_life;

            if (_life < 0)
                KillMe();
            
            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {


            //batch.DrawCircle(AbsXY, 12, 12, Color.Blue, 4);
            //batch.DrawCircle(AbsXY, 8, 8, Color.Cyan, 4);
            //batch.DrawPoint(AbsXY, Color.White, 8);


            // Shadow
            //batch.DrawPoint(AbsX, AbsY + 160, Color.Black * .1f, 16);
            //batch.DrawCircle(AbsX, AbsY + 160, 16, 16, Color.Black * .1f, 8);

            //batch.End();
            //batch.Begin(SpriteSortMode.Immediate, BlendState.Additive, SamplerState.PointClamp);

            //batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - 24, AbsY - 24, 48, 48), Color.LightGreen * .8f);

            //batch.End();
            //batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);


            return base.Render(batch);
        }

        public override Node RenderAdditive(SpriteBatch batch)
        {
            Draw.Line(batch, AbsXY, _velocity * 1.5f, _angle + Geo.RAD_180, _color * 1f, _power + 6);
            Draw.Line(batch, AbsXY, _velocity * 1.5f, _angle + Geo.RAD_180, _color * 1f, _power + 4);
            Draw.Line(batch, AbsXY, _velocity * 1.5f, _angle + Geo.RAD_180, _color * 1f, _power + 2);
            Draw.Line(batch, AbsXY, _velocity * 1.5f, _angle + Geo.RAD_180, Color.White * .8f, _power);


            batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - 40, AbsY - 40, 80, 80), _color * 1f);

            return base.RenderAdditive(batch);
        }
    }
}
