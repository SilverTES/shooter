﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.Collections.Generic;
using TiledSharp;

namespace Shooter
{

    public class ScreenPlay : Node
    {
        #region Attributes

        List<Vector2> _path = new List<Vector2>();

        Hero[] _heros = new Hero[Game1.MAX_PLAYER];

        float _cameraY = 0;

        FxFog _fxFogA;
        FxFog _fxFogB;

        // Collision Grid
        Collision2DGrid _collision2DGrid;

        bool _playMusic;


        int _curWave;
        int _tempoEnemy = 1200;
        int _ticEnemy;

        int _nbPlayer = 1;

        int _hiScore = 4000;
        bool _isHisScore;

        bool _pause;
        bool _gameOver;

        bool _resetGame;
        int _tempoResetGame = 40000;
        int _ticResetGame;

        Addon.Loop _loop = new Addon.Loop();

        TmxMap _tmxMap;
        Texture2D _tileSet;

        TileMap2D _tileMap2D;
        TileMapLayer[] _layers = new TileMapLayer[10];

        float _YLayer0;

        #endregion


        public ScreenPlay(ContentManager content)
        {

            // Create CollisionGrid
            _collision2DGrid = new Collision2DGrid(Game1._screenW / 48, Game1._screenW / 48, 48); // --- VERY IMPORTANT FOR PRECISE COLLISION !!

            SetSize(Game1._screenW, Game1._screenH);


            for (int i=0; i<_nbPlayer; i++)
            {
                _heros[i] = new Hero(Game1._players[i]);
                _heros[i].AppendTo(this);
            }


            _fxFogA = new FxFog(32, AbsRect,0,4f, 80, 640, true);
            _fxFogB = new FxFog(8, AbsRect,0,6f, 640, 1280, true);


            _loop.SetLoop(0, .5f, 1.5f, .025f, Loops.PINGPONG);
            _loop.Start();


            _tmxMap = new TmxMap("Content/tilemap_level0.tmx");
            _tileSet = content.Load<Texture2D>("Image/" + _tmxMap.Tilesets[0].Name.ToString());
            //_tileSet = Content.Load<Texture2D>(_tmxMap.Tilesets[0].Image.Source.ToString());
            //_tileSet = content.Load<Texture2D>("image/tileset");

            _tileMap2D = new TileMap2D();
            _tileMap2D.Setup(new Rectangle(0, 0, Game1._screenW, Game1._screenH), _tmxMap.Width, _tmxMap.Height, _tmxMap.TileWidth, _tmxMap.TileHeight);


            Console.WriteLine("Nb layers = " + _tmxMap.Layers[0].Name);

            _layers[0] = TileMap2D.CreateLayer(_tmxMap, _tmxMap.Layers["ForeGround"], _tileSet);

            

        }

        void PopEnemy(int nbEnemy = 10)
        {
            for (int i = 0; i < nbEnemy; i++)
            {
                float x = Misc.Rng.Next(80, Game1._screenW - 80);
                float y =  -Misc.Rng.Next(240,2400);

                int level = Misc.Rng.Next(0, 4) + 1;

                new Enemy(_heros[Misc.Rng.Next(0, _nbPlayer)], level)
                    .SetPosition(x, y)
                    .AppendTo(this);
            }

        }



        public override Node Init()
        {

            //for (int i = 0; i < 5; i++)
            //{
            //    float y = Misc.Rng.Next(-100, 100);

            //    _path.Add(new Vector2(i * 100 + 100, 360 + y));

            //}

            // Play music at start !
            MediaPlayer.Play(Game1._song_Music0);
            MediaPlayer.Volume = 0.1f;
            MediaPlayer.IsRepeating = true;


            KillAll(new int[] { UID.Get<Enemy>(), UID.Get<Bullet>() });

            PopEnemy();

            for (int i = 0; i < _nbPlayer; i++)
            {
                _heros[i].Init().SetPosition(Game1._screenW / (_nbPlayer+1) * (i+1), Game1._screenH - 120);
                
            }

            _curWave = 1;

            _isHisScore = false;

            _gameOver = false;

            _YLayer0 = -_tmxMap.Height * _tmxMap.TileHeight;

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _loop.Update();


            if (Input.Button.OnePress("Add", Game1._mouseState.LeftButton == ButtonState.Pressed))
            {
                Console.WriteLine("Add Something !");

                new Star(this, Color.Yellow, 200, 4, 6, (float)(Misc.Rng.NextDouble() * Geo.RAD_360), -.05f)
                    .SetPosition(Game1._relMouseX, Game1._relMouseY)
                    .AppendTo(this);
            }

            if (Input.Button.OnePress("PlayMusic", Keyboard.GetState().IsKeyDown(Keys.M)))
            {
                _playMusic = !_playMusic;

                if (_playMusic)
                {
                    //MediaPlayer.Play(_listSong[Misc.Rng.Next(0, _listSong.Count - 1)]); // start menu music !
                    MediaPlayer.Play(Game1._song_Music0);
                    MediaPlayer.Volume = 0.1f;
                    MediaPlayer.IsRepeating = true;
                }
                else
                {
                    MediaPlayer.Stop();
                }

            }

            // reset game
            if (Input.Button.OnePress("ResetGame", Game1.Player1.GetButton((int)SNES.BUTTONS.SELECT)!=0))
                Init();

            if (Input.Button.OnePress("Pause", Game1.Player1.GetButton((int)SNES.BUTTONS.START)!=0) && !_resetGame)
            {
                _pause = !_pause;

                if (_pause)
                    MediaPlayer.Volume = .02f;
                else
                    MediaPlayer.Volume = .1f;
            }


            if (!_pause)
            {
                ++_ticEnemy;

                _cameraY += .5f;

                _YLayer0 += 2f;

                UpdateChilds(gameTime);

                // Collision2D System Run !
                _collision2DGrid.SetPosition(0, 0);
                Collision2D.ResetAllZone(this);
                Collision2D.GridSystemZone(this, _collision2DGrid);

                _fxFogA.Update();
                _fxFogB.Update();

            }

            if (_ticEnemy > _tempoEnemy)
            {
                _ticEnemy = 0;

                Console.WriteLine("New enemies");

                _curWave++;

                PopEnemy(8 + _curWave * 8);
            }


            if (_cameraY > Game1._screenH)
            {
                _cameraY = 0;
            }

            // Debug : when hero lose init stage

            for (int i = 0; i < _nbPlayer; i++)
            {
                if (_heros[i]._energy <= 0)
                {
                    _resetGame = true;
                    _heros[i].IS_CONTROLL = false;

                    _heros[i].SetVisible(false);

                    _gameOver = true;

                    //new Explosion(Color.White, 2, true)
                    //    .SetPosition(_heros[i]._x, _heros[i]._y)
                    //    .AppendTo(_heros[i]._parent);
                    //    //.AppendTo(this);

                }

                if (_heros[i]._score > _hiScore)
                {
                    if (!_isHisScore)
                    {
                        Game1._sound_Success.Play(.4f, .01f, 0f);
                    }

                    _hiScore = _heros[i]._score;

                    _isHisScore = true;

                }

            }

            if (_resetGame)
            {
                ++_ticResetGame;
                if (_ticResetGame > _tempoResetGame)
                {
                    _ticResetGame = 0;
                    _resetGame = false;
                    Init();

                }

                if (Input.Button.OnePress("Retry", Game1.Player1.GetButton((int)SNES.BUTTONS.START) != 0))
                {
                    _resetGame = false;
                    Init();
                }

            }



            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            batch.GraphicsDevice.Clear(new Color(20, 20, 20));

            


            // Debug : Simulate scrolling
            batch.Draw(Game1._tex_Ground0, new Rectangle(0, (int)_cameraY, Game1._screenW,  Game1._screenH), Color.White);
            batch.Draw(Game1._tex_Ground0, new Rectangle(0, (int)_cameraY - Game1._screenH, Game1._screenW,  Game1._screenH), Color.White);

            Draw.RectFill(batch, AbsRect, Color.Black * .6f);

            // Grid 64x64
            //Retro2D.Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 64, 64, Color.WhiteSmoke * .8f);

            

            //batch.Draw(Game1._tex_Boss, new Vector2(Game1._screenW / 2, 80), new Rectangle(0,0,160,120),Color.White);

            //for (int i=0; i<_path.Count; i++)
            //{
            //    batch.DrawCircle(_path[i], 8, 16, Color.Orange, 2);

            //    if (i < _path.Count - 1)
            //        batch.DrawLine(_path[i], _path[i+1], Color.Red, 2);
            //}

            batch.End();
            batch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp);
            _fxFogA.Render(batch, Color.White * .3f);
            batch.End();
            batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);


            _tileMap2D.Render(batch, _layers[0], Color.White, 0, (int)_YLayer0);

            SortZD();
            RenderChilds(batch);


            

            if (_pause)
            {
                Draw.RectFill(batch, AbsRect, Color.Blue * .5f);
                Draw.RectFill(batch, new Rectangle(0, AbsRect.Height / 2 - 80, AbsRect.Width, 80*2), Color.Blue * .4f);
                Draw.CenterStringXY(batch, Game1._font_Big, "P A U S E", AbsRect.Width / 2, AbsRect.Height / 2, Color.MonoGameOrange);
            }

            if (_gameOver)
            {
                Draw.RectFill(batch, new Rectangle(0, AbsRect.Height / 2 - 80, AbsRect.Width, 80*2), Color.Blue * .4f);
                Draw.CenterStringXY(batch, Game1._font_Big, "G A M E  O V E R", AbsRect.Width / 2, AbsRect.Height / 2 - 20, Color.MonoGameOrange);
                Draw.CenterStringXY(batch, Game1._font_Main, "< Press Start >", AbsRect.Width / 2, AbsRect.Height / 2 + 40, Color.LightSteelBlue * _loop._current);
            }

            //batch.DrawLine(_hero.AbsXY, _hero2.AbsXY, Color.Red, 1f);
            //batch.DrawPoint(Gfx.GetCenter(_hero.AbsXY, _hero2.AbsXY), Color.Yellow, 4f);

            //Retro2D.Draw.Sight(batch, Game1._relMouseX, Game1._relMouseY, Game1._screenW, Game1._screenH, Color.OrangeRed * .8f, 1);
            batch.Draw(Draw._mouseCursor, new Vector2(Game1._relMouseX, Game1._relMouseY), Color.Yellow);


            for (int i = 0; i < _nbPlayer; i++)
            {
                Draw.TopCenterString(batch, Game1._font_Main, "- " + _heros[i]._score + " -", Game1._screenW / (_nbPlayer + 1) * (i + 1), 24, Color.Yellow);
                Draw.TopCenterString(batch, Game1._font_Main, "P"+ (i + 1)+" LIFE:" + _heros[i]._energy + " LVL:" + _heros[i]._powerLevel, Game1._screenW / (_nbPlayer + 1) * (i + 1), Game1._screenH - 40, Color.Yellow);
            }

            Draw.TopCenterString(batch, Game1._font_Main, "* " + _hiScore + " *", Game1._screenW/2, 2, Color.RoyalBlue);
            Draw.TopCenterString(batch, Game1._font_Main, "Wave " + _curWave, Game1._screenW/2, Game1._screenH - 20, Color.MonoGameOrange);


            return base.Render(batch);
        }

        public override Node RenderAdditive(SpriteBatch batch)
        {

            RenderAdditiveChilds(batch);

            return base.RenderAdditive(batch);
        }

        public override Node RenderAlphaBlend(SpriteBatch batch)
        {

            
            _fxFogB.Render(batch, Color.White * .1f);

            return base.RenderAlphaBlend(batch);
        }

    }

}
