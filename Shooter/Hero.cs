﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;
using System.Collections.Generic;

namespace Shooter
{
    public class Hero : Node
    {

        #region

        public const int ZONE_BODY = 0;

        List<Collide.Zone> _listZone;

        Shake _shake = new Shake();

        Player _player;

        Addon.Loop _loop = new Addon.Loop();

        public bool IS_CONTROLL = true;

        public bool IS_B_SELECT = false;
        public bool IS_B_START = false;

        public bool IS_B_UP = false;
        public bool IS_B_DOWN = false;
        public bool IS_B_LEFT = false;
        public bool IS_B_RIGHT = false;
        public bool IS_B_L = false;
        public bool IS_B_R = false;
        public bool IS_B_A = false;
        public bool IS_B_B = false;
        public bool IS_B_X = false;
        public bool IS_B_Y = false;

        public bool IS_PUSH_B_SELECT = false;
        public bool IS_PUSH_B_START = false;

        public bool IS_PUSH_B_UP = false;
        public bool IS_PUSH_B_DOWN = false;
        public bool IS_PUSH_B_LEFT = false;
        public bool IS_PUSH_B_RIGHT = false;
        public bool IS_PUSH_B_L = false;
        public bool IS_PUSH_B_R = false;
        public bool IS_PUSH_B_A = false;
        public bool IS_PUSH_B_B = false;
        public bool IS_PUSH_B_X = false;
        public bool IS_PUSH_B_Y = false;

        public bool ON_B_SELECT = false;
        public bool ON_B_START = false;

        public bool ON_B_UP = false;
        public bool ON_B_DOWN = false;
        public bool ON_B_LEFT = false;
        public bool ON_B_RIGHT = false;
        public bool ON_B_L = false;
        public bool ON_B_R = false;
        public bool ON_B_A = false;
        public bool ON_B_B = false;
        public bool ON_B_X = false;
        public bool ON_B_Y = false;

        float _angleFrame;

        float _angleMove;
        float _speed;
        float _velocity;
        float _acceleration;
        float _vx;
        float _vy;
        bool _isMove;
        

        bool _fire;
        bool _autoFire;
        int _ticFire;

        public int _powerLevel { get; private set; }

        public int _energy { get; private set; }

        public int _score { get; private set; }

        Addon.Loop _loop0 = new Addon.Loop();

        Position _curDirection;
        Position _prevDirection;

        #endregion

        public void AddScore(int points)
        {
            _score += points;
        }

        public Hero(Player player)
        {
            _type = UID.Get<Hero>();

            _speed = 12;
            _player = player;
            _acceleration = 1f;

            

            _loop.SetLoop(0, 0, Game1._anim_Jet._frames.Count - 1, .4f, Loops.PINGPONG);
            _loop.Start();

            _loop0.SetLoop(0f, 16f, 24f, .5f, Loops.PINGPONG);
            _loop0.Start();

            SetSize(64, 64);
            SetPivot(Position.CENTER);

            SetCollideZone(ZONE_BODY, Rectangle.Empty);
        }

        public void FireA(float speed, float x, float y, int power, float angle, Color color, int nb = 1, bool random = true, int variation = 100)
        {
            _fire = true;

            for (int i=0; i<nb; i++)
            {
                angle = angle + (random ? (Misc.Rng.Next(-variation, variation) / 1000f) : 0);

                new Bullet(this, color, 60, power, speed, angle, 8)
                    .SetPosition(x, y)
                    .AppendTo(_parent);
            }
        }
        public void Fire(float speed, float x, float y, int power, float angle, Color color, float[] angles)
        {
            _fire = true;

            for (int i=0; i<angles.Length; i++)
            {
                new Bullet(this, color, 60, power, speed, angles[i], 8)
                    .SetPosition(x, y)
                    .AppendTo(_parent);
            }
        }

        public override Node Init()
        {
            _energy = 4;
            _score = 0;
            _powerLevel = 0;

            IS_CONTROLL = true;
            SetVisible(true);

            return base.Init();
        }

        public void Move(float angleMove, float velocity)
        {
            _vx = Geo.GetVector(angleMove).X * velocity;
            _vy = Geo.GetVector(angleMove).Y * velocity;
        }

        public void HitDamage()
        {
            _shake.SetIntensity(4, .01f);

            _energy -= 1;

            if (_energy > 0)
            {
                new PopInfo("-1", Color.Red, Color.Red, 0, 24)
                    .SetPosition(_x, _y)
                    .AppendTo(_parent);
            }
        }

        public override Node Update(GameTime gameTime)
        {

            _z = -(int)_y;

            _loop.Update();
            _loop0.Update();

            UpdateCollideZone(ZONE_BODY, _rect);

            _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_BODY), new int[] { UID.Get<Bullet>(), UID.Get<Enemy>() }, Bullet.ZONE_BODY);

            if (_listZone.Count > 0)
            {
                bool playBulletHit = false;

                for (int i = 0; i < _listZone.Count; i++)
                {
                    Node collider = _listZone[i]._node;

                    if (collider._type == UID.Get<Bullet>())
                    {
                        Bullet bullet = collider.This<Bullet>();
                        //Console.WriteLine("Collide by Bullet !");

                        if (bullet._owner._type == UID.Get<Enemy>() && !_shake.IsShake())
                        {
                            collider.KillMe();

                            HitDamage();

                            playBulletHit = true;

                        }

                    }
                    if (collider._type == UID.Get<Enemy>())
                    {
                        Enemy enemy = collider.This<Enemy>();
                        //Console.WriteLine("Collide by Bullet !");

                        if (!_shake.IsShake())
                        {
                            enemy.Explose();

                            HitDamage();

                            playBulletHit = true;

                        }

                    }



                }

                if (playBulletHit)
                {
                    Game1._sound_Hit1.Play(.4f, .001f, 0f);
                    Game1._sound_Alert.Play(.4f, .001f, 0f);
                }

            }

            #region Button Events

            // Manage Button Status

            IS_B_UP = _player.GetButton((int)SNES.BUTTONS.UP) != 0 && IS_CONTROLL;
            IS_B_DOWN = _player.GetButton((int)SNES.BUTTONS.DOWN) != 0 && IS_CONTROLL;
            IS_B_LEFT = _player.GetButton((int)SNES.BUTTONS.LEFT) != 0 && IS_CONTROLL;
            IS_B_RIGHT = _player.GetButton((int)SNES.BUTTONS.RIGHT) != 0 && IS_CONTROLL;

            IS_B_L = _player.GetButton((int)SNES.BUTTONS.L) != 0 && IS_CONTROLL;
            IS_B_R = _player.GetButton((int)SNES.BUTTONS.R) != 0 && IS_CONTROLL;

            IS_B_A = _player.GetButton((int)SNES.BUTTONS.A) != 0 && IS_CONTROLL;
            IS_B_B = _player.GetButton((int)SNES.BUTTONS.B) != 0 && IS_CONTROLL;
            IS_B_X = _player.GetButton((int)SNES.BUTTONS.X) != 0 && IS_CONTROLL;
            IS_B_Y = _player.GetButton((int)SNES.BUTTONS.Y) != 0 && IS_CONTROLL;


            // Manage Button Trigger
            ON_B_UP = false; if (!IS_B_UP) IS_PUSH_B_UP = false; if (IS_B_UP && !IS_PUSH_B_UP) { IS_PUSH_B_UP = true; ON_B_UP = true; }
            ON_B_DOWN = false; if (!IS_B_DOWN) IS_PUSH_B_DOWN = false; if (IS_B_DOWN && !IS_PUSH_B_DOWN) { IS_PUSH_B_DOWN = true; ON_B_DOWN = true; }
            ON_B_LEFT = false; if (!IS_B_LEFT) IS_PUSH_B_LEFT = false; if (IS_B_LEFT && !IS_PUSH_B_LEFT) { IS_PUSH_B_LEFT = true; ON_B_LEFT = true; }
            ON_B_RIGHT = false; if (!IS_B_RIGHT) IS_PUSH_B_RIGHT = false; if (IS_B_RIGHT && !IS_PUSH_B_RIGHT) { IS_PUSH_B_RIGHT = true; ON_B_RIGHT = true; }

            ON_B_L = false; if (!IS_B_L) IS_PUSH_B_L = false; if (IS_B_L && !IS_PUSH_B_L) { IS_PUSH_B_L = true; ON_B_L = true; }
            ON_B_R = false; if (!IS_B_R) IS_PUSH_B_R = false; if (IS_B_R && !IS_PUSH_B_R) { IS_PUSH_B_R = true; ON_B_R = true; }

            ON_B_A = false; if (!IS_B_A) IS_PUSH_B_A = false; if (IS_B_A && !IS_PUSH_B_A) { IS_PUSH_B_A = true; ON_B_A = true; }
            ON_B_B = false; if (!IS_B_B) IS_PUSH_B_B = false; if (IS_B_B && !IS_PUSH_B_B) { IS_PUSH_B_B = true; ON_B_B = true; }
            ON_B_X = false; if (!IS_B_X) IS_PUSH_B_X = false; if (IS_B_X && !IS_PUSH_B_X) { IS_PUSH_B_X = true; ON_B_X = true; }
            ON_B_Y = false; if (!IS_B_Y) IS_PUSH_B_Y = false; if (IS_B_Y && !IS_PUSH_B_Y) { IS_PUSH_B_Y = true; ON_B_Y = true; }

            #endregion

            _vx = 0;
            _vy = 0;
            //_velocity = 0;


            if (IS_B_UP)
            {
                //_velocity += .5f;
                _curDirection = Position.UP;
                _prevDirection = _curDirection;
            }
            if (IS_B_DOWN)
            {
                //_velocity += .5f;
                _curDirection = Position.DOWN;
                _prevDirection = _curDirection;
            }
            if (IS_B_LEFT)
            {
                _angleFrame += -.5f;
                //_velocity += .5f;
                _curDirection = Position.LEFT;
                _prevDirection = _curDirection;
            }
            if (IS_B_RIGHT)
            {
                _angleFrame += .5f;
                //_velocity += .5f;
                _curDirection = Position.RIGHT;
                _prevDirection = _curDirection;
            }

            if (!(IS_B_UP || IS_B_DOWN || IS_B_LEFT || IS_B_RIGHT))
            {
                _velocity -= _acceleration * 2f;

                if (_velocity <= 0) _velocity = 0;
            }
            else
            {
                _velocity += _acceleration;
            }

            if (_velocity > _speed) _velocity = _speed;

            _isMove = false;

            if (IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_U, _velocity);
            if (!IS_B_UP && IS_B_DOWN && !IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_D, _velocity);
            if (!IS_B_UP && !IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_L, _velocity);
            if (!IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_R, _velocity);

            if (IS_B_UP && !IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_UL, _velocity);
            if (IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_UR, _velocity);
            if (!IS_B_UP && IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_DL, _velocity);
            if (!IS_B_UP && IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_DR, _velocity);

            _x += _vx;
            _y += _vy;

            if (!(IS_B_LEFT || IS_B_RIGHT))
            {
                if (_angleFrame < 5) _angleFrame += .5f;
                if (_angleFrame > 5) _angleFrame += -.5f;
            }


            if (IS_B_A)
            {
                _ticFire++;

                _fire = false;

                if (_ticFire > 5)
                {
                    _ticFire = 0;

                    _autoFire = true;

                    FireA(48, _x, _y - 48, _powerLevel + 2, -Geo.RAD_90, Color.Red, _powerLevel + 1, true);

                    //Fire(48, _x, _y - 48, _powerLevel + 2, -Geo.RAD_90, Color.Red, new float[] { -Geo.RAD_90, -Geo.RAD_90 - .05f, -Geo.RAD_90 + .05f });

                    Game1._sound_LaserGun.Play(.01f, .1f, 0f);
                }
            }
            else
            {
                _fire = false;
                _autoFire = false;
                _ticFire = 0;
            }

            if (ON_B_A && !_autoFire)
            {
                FireA(32, _x, _y - 48, _powerLevel + 1, -Geo.RAD_90, Color.BlueViolet, _powerLevel + 1, true, 40 + _powerLevel * 4);

                FireA(20, _x + 48, _y, _powerLevel, -Geo.RAD_45, Color.Blue, _powerLevel, true, 200);
                FireA(20, _x - 48, _y, _powerLevel, -Geo.RAD_135, Color.Blue, _powerLevel, true, 200);

                Game1._sound_LaserGun.Play(.01f, .1f, 0f);

                //GamePad.SetVibration(PlayerIndex.Four, 1f, 1f);
            }

            //int min = 0;
            //int max = Game1._anim_Jet._frames.Count - 1;
            int min = 2;
            int max = Game1._anim_Jet._frames.Count - 3;


            if (_angleFrame < min)
                _angleFrame = min;

            if (_angleFrame > max)
                _angleFrame = max;


            if (_x < 0) _x = 0;
            if (_x > Game1._screenW) _x = Game1._screenW;
            if (_y < 0) _y = 0;
            if (_y > Game1._screenH) _y = Game1._screenH;

            UpdateRect();


            if (_powerLevel == 0 && _score > 200)
            {
                _powerLevel = 1;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 1 && _score > 800)
            {
                _powerLevel = 2;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 2 && _score > 4800)
            {
                _powerLevel = 3;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 3 && _score > 9600)
            {
                _powerLevel = 4;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 4 && _score > 24000)
            {
                _powerLevel = 5;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 5 && _score > 48000)
            {
                _powerLevel = 6;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 6 && _score > 96000)
            {
                _powerLevel = 7;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }
            
            if (_powerLevel == 7 && _score > 120000)
            {
                _powerLevel = 8;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 8 && _score > 160000)
            {
                _powerLevel = 9;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }

            if (_powerLevel == 9 && _score > 200000)
            {
                _powerLevel = 10;
                Game1._sound_LevelUp.Play(.2f, .1f, 0f);
            }


            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            Vector2 shakeV = _shake.GetVector2();

            //int indexAnim = (int)Math.Floor(_loop._current);

            //Game1._anim_Jet.Draw(batch, indexAnim, Game1._relMouseX, Game1._relMouseY + 160, Color.Black * .1f);
            //Game1._anim_Jet.Draw(batch, indexAnim, Game1._relMouseX, Game1._relMouseY, Color.White);

            // Shadow
            Game1._anim_Jet.Draw(batch, (int)_angleFrame, AbsX, AbsY + 120, Color.Black * .1f, .8f, .8f);

            //batch.Draw(Game1._tex_Magic1, Gfx.TranslateRect(new Rectangle(AbsX - 128, AbsY - 128, 256, 256), new Point((int)shakeV.X, (int)shakeV.Y)), Color.Yellow * (.8f + (_loop0._current/100)));
            
            // Jet
            Game1._anim_Jet.Draw(batch, (int)_angleFrame, AbsX + shakeV.X, AbsY + shakeV.Y, Color.White);

            batch.End();
            batch.Begin(SpriteSortMode.Immediate, BlendState.Additive, SamplerState.PointClamp);

            //Game1._window._graphics.GraphicsDevice.BlendState = BlendState.Additive;

            //batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - 24, AbsY - 24, 48, 48), Color.LightGreen * .8f);

            if (_fire)
                batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - 80, AbsY - 80 - 48, 160, 160), Color.LightGreen * .6f);

            int r = (int)_loop0._current;

            int ox = (int)Math.Abs(_angleFrame-5)*2;

            batch.Draw(Game1._tex_Glow1, new Rectangle(-10 + ox + AbsX - r, AbsY - r + 32, r * 2, r * 2), Color.Yellow * 1f);
            batch.Draw(Game1._tex_Glow1, new Rectangle(-10 + ox + AbsX - r * 2, AbsY - r * 2 + 32, r * 4, r * 4), Color.Red * .8f);

            batch.Draw(Game1._tex_Glow1, new Rectangle(10 - ox + AbsX - r, AbsY - r + 32, r * 2, r * 2), Color.Yellow * 1f);
            batch.Draw(Game1._tex_Glow1, new Rectangle( 10 - ox + AbsX - r * 2, AbsY - r * 2 + 32, r * 4, r * 4), Color.Red * .8f);

            batch.End();
            batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);

            //Game1._window._graphics.GraphicsDevice.BlendState = BlendState.NonPremultiplied;

            //Draw.CenterStringXY(batch, Game1._font_Big, _energy.ToString(), AbsX, AbsY + 80, Color.Yellow);
            //Draw.CenterStringXY(batch, Game1._font_Big, _score.ToString(), AbsX, AbsY + 100, Color.Orange);

            // Draw Collide Zone
            //batch.DrawRectangle(GetCollideZone(ZONE_BODY)._rect, Color.Turquoise, 1);


            if (_shake.IsShake())
            {
                batch.Draw(Game1._tex_Magic1, Gfx.TranslateRect(new Rectangle(AbsX - 128, AbsY - 128, 256, 256), new Point((int)shakeV.X, (int)shakeV.Y)), Color.Violet);
            }


            return base.Render(batch);
        }

        public override Node RenderAdditive(SpriteBatch batch)
        {
            Vector2 shakeV = _shake.GetVector2();

            //if (_fire)
            //    batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - 80, AbsY - 80 - 48, 160, 160), Color.LightGreen * .6f);

            //int r = (int)_loop0._current;
            //batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - r, AbsY - r + 32, r * 2, r * 2), Color.Yellow * 1f);
            //batch.Draw(Game1._tex_Glow1, new Rectangle(AbsX - r * 2, AbsY - r * 2 + 32, r * 4, r * 4), Color.Red * .8f);

            

            if (_shake.IsShake())
            {
                batch.Draw(Game1._tex_Magic1, Gfx.TranslateRect(new Rectangle(AbsX - 128, AbsY - 128, 256, 256), new Point((int)shakeV.X, (int)shakeV.Y)), Color.BlueViolet);
            }

            return base.RenderAdditive(batch);
        }

        public void Move()
        {

        }

    }
}
