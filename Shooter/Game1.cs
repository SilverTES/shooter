﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.Collections.Generic;
using static Retro2D.Node;

namespace Shooter
{
    public class Game1 : Game
    {
        #region Attributes


        public static Window _window = new Window();
        FrameCounter _frameCounter = new FrameCounter();
        public static MouseState _mouseState;

        public const int PLAYER1 = 0;
        public const int PLAYER2 = 1;
        public const int PLAYER3 = 2;
        public const int PLAYER4 = 3;
        public const int MAX_PLAYER = 4;

        public static Player[] _players = new Player[MAX_PLAYER];
        // Alias for _players[]
        public static Player Player1 { get; private set; }
        public static Player Player2 { get; private set; }
        public static Player Player3 { get; private set; }
        public static Player Player4 { get; private set; }

        static Controller[] _controllers = new Controller[MAX_PLAYER];

        public static string _pathGamePadSetup = "controllers_Setup.xml";


        public static int _relMouseX;
        public static int _relMouseY;
        public const int _screenW = 1920;
        public const int _screenH = 1080;

        public int _finalScreenW = 1920;
        public int _finalScreenH = 1080;

        public static Texture2D _tex_Boss;
        public static Texture2D _tex_AtlasJet;
        public static Texture2D _tex_Ground0;

        public static Texture2D _tex_Enemy0;
        public static Texture2D _tex_EnemyA;
        public static Texture2D _tex_EnemyB;
        public static Texture2D _tex_EnemyC;

        public static Texture2D _tex_Magic1;
        public static Texture2D _tex_Glow0;
        public static Texture2D _tex_Glow1;
        public static Texture2D _tex_CircleGlow1;

        public static Texture2D _tex_Cloud1;
        public static Texture2D _tex_Cloud2;

        public static Texture2D _tex_Explosion;

        public static SoundEffect _sound_LaserGun;
        public static SoundEffect _sound_Hit0;
        public static SoundEffect _sound_Explosion0;
        public static SoundEffect _sound_Explosion1;
        public static SoundEffect _sound_Hit1;
        public static SoundEffect _sound_Hit2;
        public static SoundEffect _sound_Alert;
        public static SoundEffect _sound_Success;
        public static SoundEffect _sound_LevelUp;

        public static Song _song_Music0;

        public static Animation _anim_Jet;
        public static Animation _anim_Explosion;

        public static SpriteFont _font_Main;
        public static SpriteFont _font_Big;
        public static SpriteFont _font_Mini;


        #endregion


        public static readonly BlendState MaxBlend = new BlendState()
        {
            AlphaBlendFunction = BlendFunction.Max,
            ColorBlendFunction = BlendFunction.Max,
            AlphaDestinationBlend = Blend.One,
            AlphaSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,
            ColorSourceBlend = Blend.One
        };


        public Game1()
        {
            _window.Setup(this, Mode.RETRO, "Minimal", _screenW, _screenH, 2, 0, false, true, false);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            _window.Init();
            _window.SetScale(1);

            _window.SetFinalScreenSize(_finalScreenW, _finalScreenH);

            // HttpClient is intended to be instantiated once per application, rather than per-use. See Remarks.
            //static readonly HttpClient client = new HttpClient();



            base.Initialize();


            Player1 = _players[PLAYER1] = new Player(0, "Mugen");
            Player2 = _players[PLAYER2] = new Player(0, "Silver");
            Player3 = _players[PLAYER3] = new Player(0, "Zero");
            Player4 = _players[PLAYER4] = new Player(0, "Alpha");

            _controllers[PLAYER1] = new Controller()
                .AsMainController(Player1);

            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.Up))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.Down))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.Left))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.Right))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.RightAlt))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space))

                .SetButton(new Button((int)SNES.BUTTONS.START, (int)Keys.Enter))
                .SetButton(new Button((int)SNES.BUTTONS.SELECT, (int)Keys.Back))
                .AppendTo(_players[PLAYER1]);

            _controllers[PLAYER2] = new Controller()
                .AsMainController(Player2);

            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.Z))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.S))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.Q))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.D))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.V))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.B))
                .AppendTo(_players[PLAYER2]);

            _controllers[PLAYER3] =
            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.O))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.L))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.K))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.M))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.NumPad4))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space))
                //.AppendTo(_players[PLAYER3]);
                .AsMainController(Player3);

            _controllers[PLAYER4] =
            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.T))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.G))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.F))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.H))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.NumPad6))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space))
                //.AppendTo(_players[PLAYER4]);
                .AsMainController(Player4);

            // Load GamePad Setup from file
            LoadGamePadSetupFromFile(_pathGamePadSetup);


            _root["Play"] = new ScreenPlay(Content)
                .SetSize(_screenW, _screenH)
                .Init()
                .AppendTo(_root);

            Screen.Init(_root["Play"]);



        }
        public static void SaveGamePadSetupToFile(string path)
        {
            List<Controller> controllers = new List<Controller>()
            {
                _controllers[PLAYER1],
                _controllers[PLAYER2],
                _controllers[PLAYER3],
                _controllers[PLAYER4]
            };

            FileIO.XmlSerialization.WriteToXmlFile(path, controllers);

            Console.WriteLine("GamePad File Setup Saved !");
        }

        public static void LoadGamePadSetupFromFile(string path)
        {
            List<Controller> controllers = FileIO.XmlSerialization.ReadFromXmlFile<List<Controller>>(path);

            for (int i = 0; i < MAX_PLAYER; i++)
            {
                _controllers[i].Copy(controllers[i]);
            }
            Console.WriteLine("GamePad File Setup Loaded !");
        }

        protected override void LoadContent()
        {
            #region LoadContents

            _font_Main = Content.Load<SpriteFont>("Font/mainFont");
            _font_Big = Content.Load<SpriteFont>("Font/bigFont");
            _font_Mini = Content.Load<SpriteFont>("Font/miniFont");


            _tex_AtlasJet = Content.Load<Texture2D>("image/atlas_jet");
            _tex_Boss = Content.Load<Texture2D>("image/shmup_boss");
            _tex_Ground0 = Content.Load<Texture2D>("image/ground0");

            _tex_Magic1 = Content.Load<Texture2D>("image/magic1");

            _tex_Glow0 = Content.Load<Texture2D>("image/glow0");
            _tex_Glow1 = Content.Load<Texture2D>("image/glow1");
            _tex_CircleGlow1 = Content.Load<Texture2D>("image/circleGlow1");

            _tex_Cloud1 = Content.Load<Texture2D>("image/cloud_A");
            _tex_Cloud2 = Content.Load<Texture2D>("image/cloud_PNG28");

            _tex_Explosion = Content.Load<Texture2D>("image/explosion");

            _tex_Enemy0 = Content.Load<Texture2D>("image/Enemy0");
            _tex_EnemyA = Content.Load<Texture2D>("image/EnemyA");
            _tex_EnemyB = Content.Load<Texture2D>("image/EnemyB");
            _tex_EnemyC = Content.Load<Texture2D>("image/EnemyC");

            _sound_LaserGun = Content.Load<SoundEffect>("Sound/laser-pistol-gun");
            _sound_Hit0 = Content.Load<SoundEffect>("Sound/ingame_door_close");
            _sound_Hit1 = Content.Load<SoundEffect>("Sound/iron_wood_slam");
            _sound_Hit2 = Content.Load<SoundEffect>("Sound/ability_fireball");
            _sound_Explosion0 = Content.Load<SoundEffect>("Sound/Explosion");
            _sound_Explosion1 = Content.Load<SoundEffect>("Sound/atom");
            _sound_Alert = Content.Load<SoundEffect>("Sound/alert_sfx3");
            _sound_Success = Content.Load<SoundEffect>("Sound/success_02");
            _sound_LevelUp = Content.Load<SoundEffect>("Sound/success_1");

            _song_Music0 = Content.Load<Song>("Song/song0");

            #endregion


            int w = 96;
            int h = 96;

            int oX = w / 2;
            int oY = h / 2;

            _anim_Jet = new Animation(_tex_AtlasJet, "turn");
            for (int i = 6; i > 1; i--)
            {
                Rectangle rect = new Rectangle(0, i * h, w, h);

                Frame frame = new Frame(rect, 2, oX, oY);

                _anim_Jet.Add(frame);
            }


            for (int i=0; i<7; i++)
            {
                Rectangle rect = new Rectangle(0, i*h, w, h);

                Frame frame = new Frame(rect, 0, oX, oY);

                frame.SetFlip((int)SpriteEffects.FlipHorizontally);

                _anim_Jet.Add(frame);
            }



            w = 256;
            h = 256;

            oX = w / 2;
            oY = h / 2;

            _anim_Explosion = new Animation(_tex_Explosion, "Explosion").SetLoop(Loops.NONE);
            for (int j=0; j<5; j++)
            {
                for (int i=0; i<7; i++)
                {
                    Rectangle rect = new Rectangle(i * w, j * h, w, h);

                    Frame frame = new Frame(rect, 0, oX, oY, new Rectangle(0, 0, w, h));

                    _anim_Explosion.Add(frame);

                }
            }


        }
        protected override void UnloadContent()
        {
        }
        protected override void Update(GameTime gameTime)
        {
            _window.GetMouse(ref _relMouseX, ref _relMouseY, ref _mouseState);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _window.UpdateStdWindowControl();


            if (Input.Button.OnePress("natif", Keyboard.GetState().IsKeyDown(Keys.D0))) _window.SetFinalScreenSize(_screenW, _screenH);
            if (Input.Button.OnePress("160x90", Keyboard.GetState().IsKeyDown(Keys.D1))) _window.SetFinalScreenSize(160, 90);
            if (Input.Button.OnePress("320x180", Keyboard.GetState().IsKeyDown(Keys.D2))) _window.SetFinalScreenSize(320, 180);
            if (Input.Button.OnePress("640x360", Keyboard.GetState().IsKeyDown(Keys.D3))) _window.SetFinalScreenSize(640, 360);
            if (Input.Button.OnePress("960x540", Keyboard.GetState().IsKeyDown(Keys.D4))) _window.SetFinalScreenSize(960, 540);
            if (Input.Button.OnePress("1280x720", Keyboard.GetState().IsKeyDown(Keys.D5))) _window.SetFinalScreenSize(1280, 720);
            if (Input.Button.OnePress("1600x900", Keyboard.GetState().IsKeyDown(Keys.D6))) _window.SetFinalScreenSize(1600, 900);
            if (Input.Button.OnePress("1920x1080", Keyboard.GetState().IsKeyDown(Keys.D7))) _window.SetFinalScreenSize(1920, 1080);

            if (Input.Button.OnePress("init", Keyboard.GetState().IsKeyDown(Keys.F1))) _root["Play"].Init();


            _frameCounter.Update(gameTime);

            Window.Title = "Minimal Code FPS :" + _frameCounter.Fps() + " FinalScreen : " + _window.FinalScreenW + " x " + _window.FinalScreenH + " Zoom X " + _window.Scale + "  Ratio :" + (float)_window.ScreenW / (float)_window.FinalScreenW;

            Retro2D.Screen.Update(gameTime);

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            // Set Target to MainRenderTarget !
            _window.SetRenderTarget(_window.NativeRenderTarget);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);
            // Draw something here !
            Retro2D.Screen.Render(_window.Batch);
            _window.Batch.End();

            _window.Batch.Begin(SpriteSortMode.Deferred, MaxBlend, SamplerState.PointClamp);
            Retro2D.Screen.RenderAdditive(_window.Batch);

            if (Input.Button.OnePress("Thunder", Keyboard.GetState().IsKeyDown(Keys.Space)))
                _window.Render(_window.NativeRenderTarget, Color.White);

            _window.Batch.End();

            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            Retro2D.Screen.RenderAlphaBlend(_window.Batch);
            _window.Batch.End();

            // Render MainTarget in FinalTarget
            _window.SetRenderTarget(_window.FinalRenderTarget);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);
            _window.RenderMainTarget(Color.White);
            _window.Batch.DrawString(_font_Mini, "Final: " + _window.FinalScreenW + "x" + _window.FinalScreenH + " : " + _root["Play"].NbActive() + "/"+_root["Play"].NbNode() + " Free : "+ _root["Play"]._childs._freeObjects.Count, 
                new Vector2(0, 0), Color.MonoGameOrange);
            _window.Batch.End();

            // Flip to FinalRenderTarget ! 
            _window.SetRenderTarget(null);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);
            //_win.Render();
            _window.RenderFinalTarget(Color.White);
            _window.Batch.End();

            base.Draw(gameTime);
        }

    }
}