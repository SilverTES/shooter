﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace Shooter
{
    class Explosion : Node
    {

        Sprite _sprite;

        Color _color;
        float _scale;


        bool _hasHalo;
        int _radiusHalo;
        Color _colorHalo;
        float _alphaHalo;
        public Explosion(Color color, float scale = 1f, bool hasHalo = false)
        {
            _sprite = new Sprite();

            _color = color;
            _scale = scale;
            _hasHalo = hasHalo;

            SetSize(256, 256);
            SetPivot(Position.CENTER);

            _sprite.Add(Game1._anim_Explosion);

            //_sprite.SetDurationFactor(scale);

            //map2D.Get(mapX, mapY)._type = _type;
            //GoTo(mapX, mapY);

            //Game1._sound_killBlock.Play(.2f, .001f, 0);
            //Game1._sound_explosion.Play(.2f, .01f, 0);

            //SetCollideZone(0, _rect);
            Init();

            _radiusHalo = 80;
            _alphaHalo = 1f;



        }

        public override Node Init()
        {

            _sprite.Start("Explosion", 1, 0);

            return base.Init();
        }


        public override Node Update(GameTime gameTime)
        {
            UpdateRect();
            //UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(5, 10, -10, -20)));

            _z = -(int)_y - 10; // Explosion is over everything on the same line !

            _radiusHalo += 4;

            _alphaHalo -= .025f;

            if (_alphaHalo < 0) _alphaHalo = 0;

            _colorHalo = _color * _alphaHalo;

            _sprite.Update();


            //if (_radiusHalo > 320)
            if (_sprite.OffPlay)
                KillMe();

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {


            return this;
        }
                
        public override Node RenderAdditive(SpriteBatch batch)
        {
            if (_hasHalo)
            {
                batch.Draw(Game1._tex_Glow1, Gfx.TranslateRect(new Rectangle(AbsX - _radiusHalo * 2, AbsY - _radiusHalo * 2, _radiusHalo * 4, _radiusHalo * 4), new Point(0, 0)), _colorHalo * 2f);
                batch.Draw(Game1._tex_CircleGlow1, Gfx.TranslateRect(new Rectangle(AbsX - _radiusHalo * 2, AbsY - _radiusHalo * 2, _radiusHalo * 4, _radiusHalo * 4), new Point(0, 0)), _colorHalo * 2f);
            }

            _sprite.Draw(batch, AbsX, AbsY, _color * 2f, _scale, _scale);

            return base.RenderAdditive(batch);
        }

    }
}
