﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace Shooter
{
    public class PopInfo : Node
    {
        string _label;
        Color _color;
        Color _colorBG;

        public PopInfo(string label, Color color, Color colorBG, float start = 0, float end = 8, float duration = 24)
        {
            _label = label;
            _color = color;
            _colorBG = colorBG;

            _animate.Add("popup", Easing.BackEaseInOut, new Tweening(start, end, duration));
            _animate.Start("popup");

            _z = -10000; // Over all Node Childs
        }

        public override Node Update(GameTime gameTime)
        {
            UpdateRect();

            if (_animate.OnEnd("popup"))
            {
                KillMe();
            }

            _animate.NextFrame();


            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            Draw.TopCenterBorderedString(batch, Game1._font_Main, _label, AbsX, AbsY - _animate.Value(), _color, _colorBG);

            return base.Render(batch);
        }
    }
}
