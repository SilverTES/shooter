﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System.Collections.Generic;

namespace Shooter
{
    public class Enemy : Node
    {
        #region

        public const int ZONE_BODY = 0;

        List<Collide.Zone> _listZone;

        Vector2 _nextPos;

        Shake _shake = new Shake();

        int _energy;
        int _level;

        bool _collideByHeroBullet;

        float _vx;
        float _vy;

        Hero _heroTarget;

        Texture2D _tex;

        int _tempoShoot = 80;
        int _ticShoot;

        int _tempoReloadArm = 4;
        int _ticReloadArm;

        int _ticHit;
        int _tempoHit = 4;
        bool _isHit;

        #endregion



        public Enemy(Hero heroTarget, int level = 1)
        {

            _type = UID.Get<Enemy>();

            _heroTarget = heroTarget;

            SetCollideZone(ZONE_BODY, RectangleF.Empty);

            _level = level;

            _tex = Game1._tex_EnemyA;

            if (_level > 1) _tex = Game1._tex_Enemy0;
            if (_level > 2) _tex = Game1._tex_EnemyC;
            if (_level > 3) _tex = Game1._tex_EnemyB;
            

            _energy = _level * 2;

            if (_level > 3) _energy = _level * 5;

            _vy = 6 - _level;


            int dx = Misc.Rng.Next(-2, 2);
            _vx = (_vy/10) * dx;

            SetSize(40 + _level * 40, 40 + _level * 30);
            SetPivot(Position.CENTER);
        }

        public override Node Init()
        {

            return base.Init();
        }

        public void Explose()
        {
            Color colorExplosion = Color.SeaGreen;

            if (_level > 1) colorExplosion = Color.Red;
            if (_level > 2) colorExplosion = Color.DarkViolet;
            if (_level > 3) colorExplosion = Color.LightSteelBlue;

            new Explosion(colorExplosion, (float)_level/2f, _level > 3)
                .SetPosition(_x, _y)
                .AppendTo(_parent);

            new PopInfo("KILL", Color.Red, Color.Black, 0, 8, 64)
                .SetPosition(_x, _y)
                .AppendTo(_parent);


            // When dead pop bullet
            if (_level > 1)
            {
                float angle = Geo.GetRadian(XY, _heroTarget.XY);
                new Bullet(this, Color.Red, 400, 8, 8, angle, .05f)
                    .SetPosition(_x, _y)
                    .AppendTo(_parent);
            }

            Game1._sound_Explosion0.Play(.06f, .01f, 0f);

            if (_level > 2) Game1._sound_Hit2.Play(.06f, .01f, 0f);
            if (_level > 3) Game1._sound_Explosion1.Play(.08f, .001f, 0f);

            KillMe();
        }

        public override Node Update(GameTime gameTime)
        {

            _z = -(int)_y;

            if (_level < 2)
            {
                _vx = Geo.GetVector(XY, _heroTarget.XY, 2f).X;
            }

            _x += _vx;
            _y += _vy;

            if (_x < 80) _vx = -_vx;
            if (_x > Game1._screenW - 80) _vx = -_vx;


            if (_y > Game1._screenH + 80)
            {
                //if (null != _heroTarget)
                //    _heroTarget.HitDamage();

                //Game1._sound_Hit1.Play(.4f, .001f, 0f);
                //Game1._sound_Alert.Play(.4f, .001f, 0f);

                KillMe();
            }

            UpdateRect();

            UpdateCollideZone(ZONE_BODY, _rect);

            _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_BODY), UID.Get<Bullet>(), Bullet.ZONE_BODY);

            _collideByHeroBullet = false;

            if (_listZone.Count > 0)
            {
                bool playBulletHit = false;

                for (int i=0; i<_listZone.Count; i++)
                {
                    Node collider = _listZone[i]._node;

                    if (collider._type == UID.Get<Bullet>())
                    {
                       Bullet bullet = collider.This<Bullet>();
                        //Console.WriteLine("Collide by Bullet !");

                        if (bullet._owner._type == UID.Get<Hero>())
                        {

                            bullet._owner.This<Hero>().AddScore(_level*4);

                            _collideByHeroBullet = true;

                            collider.KillMe();

                            _shake.SetIntensity(3, .05f);

                            _energy -= 1;

                            if (_energy > 0)
                            {
                                new PopInfo("+"+ _level, Color.Orange, Color.Black, 0, 24)
                                    .SetPosition(collider._x, collider._y)
                                    .AppendTo(collider._parent);
                            }

                            playBulletHit = true;

                            _ticHit = 0;
                            _isHit = true;

                        }

                    }

                }
                if (playBulletHit)
                    Game1._sound_Hit0.Play(.08f, .001f, 0f);

            }

            if (_energy < 0)
            {
                Explose();
            }

            //if (_level > 3)
            //{
            //    ++_ticShoot;
            //    if (_ticShoot > _tempoShoot)
            //    {
            //        ++_ticReloadArm;
            //        if (_ticReloadArm > _tempoReloadArm)
            //        {
            //            new Bullet(this, Color.Yellow, 400, 4, 8, Geo.GetRadian(XY, _heroTarget.XY), .5f)
            //            .SetPosition(_x, _y)
            //            .AppendTo(_parent);

            //            Game1._sound_LaserGun.Play(.01f, .1f, 0f);

            //        }

            //        _ticShoot = 0;

            //    }

            //}

            if (_isHit)
                ++_ticHit;

            if (_ticHit > _tempoHit)
            {
                _ticHit = 0;
                _isHit = false;
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            Vector2 shakeV = _shake.GetVector2();

            Color color = Color.White;

            //if (_shake.IsShake())
            //{
            //    color = Color.Red;
            //    if (_collideByHeroBullet)
            //        color = Color.Blue;
            //}

            if (_isHit)
            {
                color = Color.Red;
                if (_collideByHeroBullet)
                    color = Color.Blue;
            }

            // Shadow
            batch.Draw(_tex, (Rectangle)Gfx.TranslateRect(Gfx.AddRect(AbsRect, new Rectangle(10, 10, -20, -20)), shakeV + new Vector2(0, 120)), Color.Black * .2f);

            batch.Draw(_tex, (Rectangle)Gfx.TranslateRect(AbsRect, shakeV), color);

            //Draw.CenterStringXY(batch, Game1._font_Big, _index + ":" + _energy, AbsX, AbsY + 80, Color.Purple);
            //Draw.CenterStringXY(batch, Game1._font_Big, _level.ToString(), AbsX, AbsY + 80, Color.Purple);

            return base.Render(batch);
        }

        public override Node RenderAdditive(SpriteBatch batch)
        {
            //Vector2 shakeV = _shake.GetVector2();

            //Color color = Color.White;

            //if (_shake.IsShake())
            //{
            //    color = Color.Red;
            //    if (_collideByHeroBullet)
            //        color = Color.Yellow;
            //}

            //if (_collideByHeroBullet ||_shake.IsShake())
            //    batch.Draw(_tex, (Rectangle)Gfx.TranslateRect(AbsRect, shakeV), color);

            return base.RenderAdditive(batch);
        }

    }
}
