﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace Shooter
{
    class FxFog
    {
        class Fog
        {
            float _width;
            float _height;

            float _rotation;
            int _flip;

            int _texId;

            Texture2D _tex2D;

            Vector2 _pos = new Vector2();
            Vector2 _dir = new Vector2();

            Rectangle _viewRect;

            public Fog(float x, float y, float dx, float dy, Rectangle viewRect, int sizeMini = 512, int sizeMaxi = 1028, bool aspectRatio = false)
            {
                _pos.X = x;
                _pos.Y = y;

                //_dir.X = dx * (float)(Misc.Rng.NextDouble());
                //_dir.Y = dy * (float)(Misc.Rng.NextDouble());

                _rotation = (float)(Misc.Rng.Next(0, 628)) / 100f;
                //_rotation = 0f;
                _flip = Misc.Rng.Next(0, 2);

                _texId = Misc.Rng.Next(0, 2);

                if (_texId == 0) _tex2D = Game1._tex_Cloud1;
                if (_texId == 1) _tex2D = Game1._tex_Cloud2;

                _dir.X = dx;
                _dir.Y = dy;

                _viewRect = viewRect;

                _width = Misc.Rng.Next(sizeMini, sizeMaxi);

                if (aspectRatio)
                    _height = _width;
                else
                    _height = Misc.Rng.Next(sizeMini, sizeMaxi);
            }

            public void Update()
            {
                _pos.X += _dir.X;
                _pos.Y += _dir.Y;

                if (_pos.X > _viewRect.X + _viewRect.Width*2 && _dir.X > 0) _pos.X = -_viewRect.Width*2;
                if (_pos.X < -_viewRect.X - _viewRect.Width*2 && _dir.X < 0) _pos.X = _viewRect.X + _viewRect.Width*2;

                if (_pos.Y > _viewRect.Y + _viewRect.Height*2 && _dir.Y > 0) _pos.Y = -_viewRect.Height*2;
                if (_pos.Y < -_viewRect.Y - _viewRect.Height*2 && _dir.Y < 0) _pos.Y = _viewRect.Y + _viewRect.Height*2;

                _rotation += _flip < 1? .001f : -.001f;

            }

            public void Render(SpriteBatch batch, Color color)
            {


                batch.Draw
                (
                    _tex2D, 
                    new Rectangle((int)_pos.X, (int)_pos.Y, (int)_width, (int)_height), 
                    _tex2D.Bounds, 
                    color, 
                    _rotation, 
                    new Vector2(_width/2, _height/2),
                    (SpriteEffects)_flip, 
                    0
                );
            }

        }

        Rectangle _viewRect;

        List<Fog> _fogs = new List<Fog>();

        public FxFog(int nbFog, Rectangle viewRect, float dx = 0, float dy = 1, int sizeMini = 512, int sizeMaxi = 1028, bool aspectRatio = false)
        {
            _viewRect = viewRect;

            for (int i = 0; i < nbFog; i++)
            {
                float x = Misc.Rng.Next(_viewRect.X - _viewRect.Width, _viewRect.X + _viewRect.Width);
                float y = Misc.Rng.Next(_viewRect.Y - _viewRect.Width, _viewRect.Y + _viewRect.Height);

                Fog fog = new Fog(x, y, dx, dy, _viewRect, sizeMini, sizeMaxi, aspectRatio);

                _fogs.Add(fog);
            }

        }

        public void Update()
        {
            for (int i = 0; i < _fogs.Count; i++)
            {
                _fogs[i].Update();
            }
        }

        public void Render(SpriteBatch batch, Color color)
        {
            for (int i = 0; i < _fogs.Count; i++)
            {
                _fogs[i].Render(batch, color);
            }

        }

    }
}
